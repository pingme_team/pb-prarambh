package com.pingbits;

import android.content.Context;
import android.content.SharedPreferences;
import android.gesture.Gesture;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.pingbits.adapters.ChatListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aagam on 21/2/15.
 */
public class SecretChatListFragment extends Fragment implements GestureOverlayView.OnGesturePerformedListener {

    public ListView lv;
    public ChatListAdapter chatListAdapter;
    public GestureLibrary gLibrary;
    public GestureOverlayView mView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_secret_chat,container,false);
        lv =(ListView) view.findViewById(R.id.secret_chat_list);
        lv.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        lv.setStackFromBottom(true);
        List<MsgObject> obj = new ArrayList<>();

        SharedPreferences preferences = getActivity().getSharedPreferences("pb", Context.MODE_PRIVATE);
        String user_name = preferences.getString("name","null1");


//        gLibrary = GestureBuilderActivity.getStore();
        mView = (GestureOverlayView) view.findViewById(R.id.gestures);
//        mView.setGestureColor(getActivity().getResources().getColor(R.color.secondary_text_color));
        mView.addOnGesturePerformedListener(this);

        chatListAdapter = new ChatListAdapter(getActivity(),obj,user_name);
        lv.setAdapter(chatListAdapter);
        return view;
    }

    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        Toast.makeText(getActivity(), "Started", Toast.LENGTH_SHORT).show();
        lv.setVisibility(View.VISIBLE);
        mView.setVisibility(View.GONE);

    }


    public void refreshView() {
        lv.setVisibility(View.GONE);
        mView.setVisibility(View.VISIBLE);
    }
}

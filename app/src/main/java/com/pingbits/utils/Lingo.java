package com.pingbits.utils;

import java.util.HashMap;

/**
 * Created by aagam on 22/2/15.
 */
public class Lingo {

    public  static HashMap<String,String> map = new HashMap<>();
    static {
        map.put("@", "at");
        map.put("b", "be");
        map.put("c", "see");
        map.put("d", "the");
        map.put("k", "okay");
        map.put("n", "and");
        map.put("q", "queue");
        map.put("r", "are");
        map.put("t", "tea");
        map.put("u", "you");
        map.put("w", "with");
        map.put("y", "why");
        map.put("b4", "before");
        map.put("bf", "boy friend");
        map.put("cn", "can");
        map.put("da", "the");
        map.put("ez", "easy");
        map.put("fb", "facebook");
        map.put("gb", "good bye");
        map.put("gf", "girl friend");
        map.put("gn", "good night");
        map.put("h8", "hate");
        map.put("hv", "have");
        map.put("hw", "homework");
        map.put("ik", "i know");
        map.put("jk", "just kidding");
        map.put("kk", "okay");
        map.put("l8", "late");
        map.put("m8", "mate");
        map.put("ne", "any");
        map.put("np", "no problem");
        map.put("ty", "thank you");
        map.put("ur", "your");
        map.put("wl", "will");
        map.put("yw", "you are welcome");
        map.put("1ce", "once");
        map.put("abt", "about");
        map.put("ack", "acknowledge");
        map.put("afk", "away from keyboard");
        map.put("aka", "also known as");
        map.put("app", "application");
        map.put("atb", "all the best");
        map.put("atm", "at the moment");
        map.put("bff", "best friend forever");
        map.put("brb", "be right back");
        map.put("brd", "bored");
        map.put("brt", "be right there");
        map.put("btw", "by the way");
        map.put("chk", "check");
        map.put("cos", "because");
        map.put("coz", "because");
        map.put("cr8", "create");
        map.put("cuz", "because");
        map.put("cya", "see you");
        map.put("der", "there");
        map.put("diy", "do it yourself");
        map.put("dnt", "don't");
        map.put("ezy", "easy");
        map.put("fab", "fabulous");
        map.put("fwd", "forward");
        map.put("fyi", "for your information");
        map.put("g2g", "got to go");
        map.put("gr8", "great");
        map.put("gud", "good");
        map.put("hak", "hugs and kisses");
        map.put("idk", "i don't know");
        map.put("ilu", "i love you");
        map.put("imo", "in my opinion");
        map.put("l8r", "later");
        map.put("lmk", "let me know");
        map.put("lol", "laugh out loud");
        map.put("luv", "love");
        map.put("msg", "message");
        map.put("ne1", "anyone");
        map.put("no1", "no one");
        map.put("nvr", "never");
        map.put("oic", "oh I see");
        map.put("omg", "oh my God");
        map.put("pls", "please");
        map.put("plz", "please");
        map.put("pov", "point of view");
        map.put("ppl", "people");
        map.put("sez", "says");
        map.put("sry", "sorry");
        map.put("sup", "what's up?");
        map.put("thx", "thanks");
        map.put("txt", "text");
        map.put("w/o", "without");
        map.put("wen", "when");
        map.put("wtf", "what the fuck");
        map.put("wth", "what the hell");
        map.put("2day", "today");
        map.put("2mor", "tomorrow");
        map.put("4get", "forget");
        map.put("asap", "as soon as possible");
        map.put("bcos", "because");
        map.put("bday", "birthday");
        map.put("cre8", "create");
        map.put("imho", "in my humble opinion");
        map.put("lmao", "laughing my ass off");
        map.put("lolz", "laughing out loud");
        map.put("njoy", "enjoy");
        map.put("noob", "newbie");
        map.put("nsfw", "not safe for work");
        map.put("rofl", "rolling on the floor laughing");
        map.put("str8", "straight");
        map.put("sum1", "someone");
        map.put("thnq", "thank you");
        map.put("thnx", "thanks");
        map.put("ttul", "talk to you later");
        map.put("ttyl", "talk to you later");
        map.put("xoxo", "hugs and kisses");
        map.put("2moro", "tomorrow");
        map.put("2nite", "tonight");
        map.put("4ever", "forever");
        map.put("afaik", "as far as i know");
        map.put("gonna", "going to");
    }

    public static String getProperLingo(String s){
        String[] arr = s.split(" ");
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            if(map.containsKey(arr[i].toLowerCase()))
                arr[i] = map.get(arr[i].toLowerCase());
            builder.append(arr[i]);
            builder.append(" ");
        }



        return builder.toString().substring(0,builder.toString().length()-1);
    }
}

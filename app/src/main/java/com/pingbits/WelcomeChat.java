package com.pingbits;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.pingbits.adapters.PagerAdapter;
import com.pingbits.adapters.ViewPagerTransformer;
import com.pingbits.intelli.Detected;
import com.pingbits.intelli.Detection;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;


public class WelcomeChat extends ActionBarActivity implements View.OnClickListener {

    public SharedPreferences preferences;

    public String user_name = "pbadmin";

    public Toolbar toolbar;

    public EditText message;

    public ListView lv;

    public ImageView sendButton;

    public PagerAdapter pagerAdapter;

    public ViewPager pager;

    public SecretChatListFragment secret;

    public ChatListFragment normal;

    private GetAsyncTask gat = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_chat);
        preferences = getSharedPreferences("pb", MODE_PRIVATE);
        user_name = preferences.getString("name", "null1");
        initGUI();
        if (gat == null) {
            gat = new GetAsyncTask(this);
            gat.execute();
        }

        // Demo usage
        Detected res = Detection.getDetected("There is a meeting in the coming week", "jaydeep");
        if (res == null) {
            Log.d("INTELLIJ", "it is null");
        } else {
            Log.d("INtELLIJ", res.toString());
        }
    }


    private void initGUI() {
        pagerAdapter = new PagerAdapter(getSupportFragmentManager());

        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(pagerAdapter);
        pager.setPageTransformer(true, new ViewPagerTransformer());
        secret = (SecretChatListFragment) pagerAdapter.getRegisteredFragment(1);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                    int positionOffsetPixels) {

            }


            @Override
            public void onPageSelected(int position) {
                if(position==1){
                    secret = (SecretChatListFragment) pagerAdapter.getRegisteredFragment(1);
                    secret.refreshView();
                }
            }


            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        message = (EditText) findViewById(R.id.editText);
        sendButton = (ImageView) findViewById(R.id.send);
        sendButton.setOnClickListener(this);

    }


    private void showDialog() {
        MaterialDialog.ButtonCallback cb = new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                EditText et = (EditText) dialog.getCustomView().findViewById(R.id.user_name);
                String user_name_entered = et.getText().toString();
                if (!user_name_entered.isEmpty()) {
                    user_name = user_name_entered;
                    preferences.edit().putString("name", user_name).apply();
                    secret = (SecretChatListFragment) pagerAdapter.getRegisteredFragment(1);
                    normal.chatListAdapter.setName(user_name);
                    secret.chatListAdapter.setName(user_name);
                }
            }


            @Override
            public void onNegative(MaterialDialog dialog) {
            }
        };

        MaterialDialog dialog = new MaterialDialog.Builder(this)
                .title("Enter your name")
                .titleColorRes(R.color.text_color)
                .customView(R.layout.item_edittext, false)
                .positiveText("Login")
                .negativeText(android.R.string.cancel)
                .positiveColorRes(R.color.primary)
                .negativeColorRes(R.color.primary)
                .callback(cb).build();
           /* EditText status = (EditText) dialog.getCustomView().findViewById(R.id.status_new);
            status.setText(statusView.getText().toString());
            status.setSelection(status.getText().length());*/
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_welcome_chat, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        if (user_name.equals("null1")) {
            showDialog();
        } else {
            String text = message.getText().toString();
            gat.cc.sendMessage(user_name, text);
            message.setText("");
        }

//        readCal(System.currentTimeMillis());

//        addDirectlyCalendar();
//        askToAddCalendar();
    }


    private void askToAddCalendar() {

        // Construct event details
        long startMillis = 0;
        long endMillis = 0;
        Calendar beginTime = Calendar.getInstance();
        beginTime.setTime(new Date());
        // beginTime.set(2015, 2, 21, 14, 30);
        startMillis = beginTime.getTimeInMillis();
        Calendar endTime = Calendar.getInstance();

        endTime.setTime(new Date());
        endTime.set(Calendar.HOUR, endTime.get(Calendar.HOUR) + 1);
        endMillis = endTime.getTimeInMillis();

        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMillis)
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endMillis)
                .putExtra(CalendarContract.Events.TITLE, "Yoga @ PB")
                .putExtra(CalendarContract.Events.DESCRIPTION, "Group class pbpbpb")
                .putExtra(CalendarContract.Events.EVENT_LOCATION, "The gym")
                .putExtra(CalendarContract.Events.AVAILABILITY,
                        CalendarContract.Events.AVAILABILITY_BUSY)
                .putExtra(Intent.EXTRA_EMAIL, "rowan@example.com,trevor@example.com");
        startActivity(intent);
    }


    public HashMap<Long, String> readCal(long t) {

        List<Events> eventsList = new ArrayList<>();

        Date d = new Date(t);

        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.set(Calendar.HOUR_OF_DAY, c.get(Calendar.HOUR_OF_DAY) - 1);

        long b = c.getTimeInMillis();
        c.set(Calendar.HOUR_OF_DAY, c.get(Calendar.HOUR_OF_DAY) + 2);
        long nx = c.getTimeInMillis();

        Uri uri = CalendarContract.Events.CONTENT_URI;
        String[] projection = new String[]{
                CalendarContract.Events._ID, CalendarContract.Events.TITLE,
                CalendarContract.Events.CALENDAR_ID
                , CalendarContract.Events.DTSTART, CalendarContract.Events.DTEND};

        /*Cursor calendarCursor = getContentResolver().query(uri, projection,
                "dtstart <= ? AND dtend >= ? AND calendar_id = ?",
                new String[]{"" + nx, "" + b, "2"}, null);*/

        Cursor calendarCursor = getContentResolver().query(uri, projection,
                "dtstart <= ? AND dtend >= ? ",
                new String[]{"" + nx, "" + b}, null);


/*//            Query to find calendar id of specific event
       Cursor calendarCursor1 = getContentResolver().query(uri, projection,
                null, null, null);


        int id = 0;
        while (calendarCursor1.moveToNext()){
            if(calendarCursor1.getString(1).contains("Tom")){
                Log.e("id",""+calendarCursor1.getInt(2));
                id = calendarCursor1.getInt(2);
            }
        }*/

        while (calendarCursor.moveToNext()) {
            Events e = new Events();
            e.setTitle(calendarCursor.getString(1));
            e.setStartTime(calendarCursor.getLong(3));
            e.setEndTime(calendarCursor.getLong(4));
            eventsList.add(e);
        }

        calendarCursor.close();

        HashMap<Long, String> timing = new HashMap<>();
        timing.put(b, "");
        timing.put(t, "");
        timing.put(nx, "");

        for (int i = 0; i < eventsList.size(); i++) {
            Events event = eventsList.get(i);

            if (event.getEndTime() < t) {
                timing.put(b, timing.get(b) + " " + event.getTitle());
            } else if (event.getEndTime() <=nx) {
                timing.put(t, timing.get(t) + " " + event.getTitle());
            } else {
                timing.put(nx, timing.get(nx) + " " + event.getTitle());
            }
        }

        return timing;
    }


    public void addDirectlyCalendar() {

        Uri uri = CalendarContract.Calendars.CONTENT_URI;
        String[] projection = new String[]{
                CalendarContract.Calendars._ID,
                CalendarContract.Calendars.ACCOUNT_NAME,
                CalendarContract.Calendars.CALENDAR_DISPLAY_NAME,
                CalendarContract.Calendars.NAME,
                CalendarContract.Calendars.CALENDAR_COLOR,
        };

        Cursor calendarCursor = getContentResolver().query(uri, projection, null, null, null);
//        calendarCursor.moveToFirst();
        while (calendarCursor.moveToNext()) {
            int id = calendarCursor.getInt(0);
            String accName = calendarCursor.getString(1);
            String caleDisplay = calendarCursor.getString(2);
            String name = calendarCursor.getString(3);
            String col = calendarCursor.getString(4);
        }

        // Construct event details
        long startMillis = 0;
        long endMillis = 0;
        Calendar beginTime = Calendar.getInstance();
        beginTime.setTime(new Date());
        // beginTime.set(2015, 2, 21, 14, 30);
        startMillis = beginTime.getTimeInMillis();
        Calendar endTime = Calendar.getInstance();

        endTime.setTime(new Date());
        endTime.set(Calendar.HOUR, endTime.get(Calendar.HOUR) + 1);
        endMillis = endTime.getTimeInMillis();

// Insert Event
        ContentResolver cr = getContentResolver();
        ContentValues values = new ContentValues();
        TimeZone timeZone = TimeZone.getDefault();
        values.put(CalendarContract.Events.DTSTART, startMillis);
        values.put(CalendarContract.Events.DTEND, endMillis);
        values.put(CalendarContract.Events.EVENT_TIMEZONE, timeZone.getID());
        values.put(CalendarContract.Events.TITLE, "Pingbits");
        values.put(CalendarContract.Events.DESCRIPTION, "Lunch at PB");
        values.put(CalendarContract.Events.CALENDAR_ID, 5);
        Uri uri1 = cr.insert(CalendarContract.Events.CONTENT_URI, values);

// Retrieve ID for new event
        String eventID = uri1.getLastPathSegment();

        calendarCursor.close();

        Log.e("eventID", eventID);
    }


    public void addNewMessages(ArrayList<MsgObject> msgs) {
        normal = (ChatListFragment) pagerAdapter.getRegisteredFragment(0);
        normal.chatListAdapter.add(msgs);
    }


}

package com.pingbits.adapters;

import android.content.Context;
import android.content.Intent;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pingbits.MsgObject;
import com.pingbits.R;
import com.pingbits.WelcomeChat;
import com.pingbits.intelli.Detected;
import com.pingbits.intelli.Detection;
import com.pingbits.utils.Lingo;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by aagam on 21/2/15.
 */
public class ChatListAdapter extends BaseAdapter {


    public LayoutInflater inflater;

    public List<MsgObject> objects;

    public String myName;

    public Context ctx;

    private SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");

    public ChatListAdapter(Context ctx, List<MsgObject> msgs, String name) {
        inflater = LayoutInflater.from(ctx);
        objects = msgs;
        this.ctx = ctx;
        myName = name;
    }


    @Override
    public int getCount() {
        return objects.size();
    }


    @Override
    public Object getItem(int position) {
        return null;
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = inflater.inflate(R.layout.item_calendar, parent, false);
        MsgObject object = objects.get(position);

        if (object.sender.equals(myName)) {
            view.findViewById(R.id.sub_layout_chat_my).setVisibility(View.VISIBLE);
            view.findViewById(R.id.sub_layout_chat).setVisibility(View.GONE);
            TextView mymessage = (TextView) view.findViewById(R.id.chat_message_my);
            mymessage.setText(object.text);

        } else if(!object.text.contains("movie")) {
            view.findViewById(R.id.sub_layout_chat).setVisibility(View.VISIBLE);
            view.findViewById(R.id.sub_layout_chat_my).setVisibility(View.GONE);
            TextView message = (TextView) view.findViewById(R.id.chat_message);
            TextView name = (TextView) view.findViewById(R.id.chat_name);
            name.setText(object.sender);
            message.setText(object.text);


            String msgLingoed = Lingo.getProperLingo(object.text);
            final Detected detected = Detection.getDetected(msgLingoed, object.sender);
            if (detected != null) {
                view.findViewById(R.id.calView).setVisibility(View.VISIBLE);
                HashMap<Long,String> map = ((WelcomeChat) ctx).readCal(detected.timestamp);
                TextView tv1,tv2,tv3,tv4,tv5,tv6;
                ImageView add;
                tv1 = (TextView)view.findViewById(R.id.firstCal);
                tv2 = (TextView)view.findViewById(R.id.firstCalValue);
                tv3 = (TextView)view.findViewById(R.id.secCal);
                tv4 = (TextView)view.findViewById(R.id.secCalValue);
                tv5 = (TextView)view.findViewById(R.id.thirdCal);
                tv6 = (TextView)view.findViewById(R.id.thirdCalValue);
                add = (ImageView)view.findViewById(R.id.addReminder);

                add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //pass starting time in millis here
                        addToCalendar(detected.timestamp,detected.title);
                    }
                });


                Set<Long> set = map.keySet();
                Long[] arr = set.toArray(new Long[3]);
                Arrays.sort(arr);
                tv1.setText(sdf.format(new Date(arr[0])));
                tv3.setText(sdf.format(new Date(arr[1])));
                tv5.setText(sdf.format(new Date(arr[2])));


                if(map.get(arr[0]).equals(""))
                    tv2.setText(" - ");
                else
                    tv2.setText(map.get(arr[0]));
                if(map.get(arr[1]).equals(""))
                {   tv3.setBackgroundResource(R.drawable.calendar_focus);
                    tv4.setText(" - ");}
                else{
                    tv3.setBackgroundResource(R.drawable.calendar_error);
                    tv4.setText(map.get(arr[1]));
                    tv4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            addToCalendar(detected.timestamp,detected.title);
                        }
                    });
                }
                if(map.get(arr[2]).equals(""))
                    tv6.setText(" - ");
                else
                    tv6.setText(map.get(arr[2]));

            } else {
                view.findViewById(R.id.calView).setVisibility(View.GONE);
            }

        } else if(object.text.contains("movie")){
            view = inflater.inflate(R.layout.item_chat_movie, parent, false);
            TextView message = (TextView) view.findViewById(R.id.chat_message);
            TextView name = (TextView) view.findViewById(R.id.chat_name);
            message.setText(object.text);
            name.setText(object.sender);
        }

        return view;
    }

    private void addToCalendar(long t,String title) {
        long startMillis = 0;
        long endMillis = 0;
        Calendar beginTime = Calendar.getInstance();
        beginTime.setTime(new Date(t));
        startMillis = beginTime.getTimeInMillis();
        Calendar endTime = Calendar.getInstance();

        endTime.setTime(new Date(t));
        endTime.set(Calendar.HOUR, endTime.get(Calendar.HOUR) + 1);
        endMillis = endTime.getTimeInMillis();

        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMillis)
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endMillis)
                .putExtra(CalendarContract.Events.TITLE, title)
                .putExtra(CalendarContract.Events.DESCRIPTION, "Added by Pingbits")
//                .putExtra(CalendarContract.Events.EVENT_LOCATION, "The gym")
                .putExtra(CalendarContract.Events.AVAILABILITY,
                        CalendarContract.Events.AVAILABILITY_BUSY);
//                .putExtra(Intent.EXTRA_EMAIL, "rowan@example.com,trevor@example.com");
        ctx.startActivity(intent);
    }


    public void add(List<MsgObject> object) {
        if (object.size() > 50) {
            int start = object.size() - 50;
            object = object.subList(start, object.size());
        }
        objects.addAll(object);
        notifyDataSetChanged();
    }


    public void setName(String name) {
        myName = name;
    }

}

package com.pingbits.intelli;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Question {

    private static String pattern = "^(is|what|when|will|who|how|had|which|whose|whom|why|whether|are)";

    private static Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

    public static boolean isQuestion(String text) {
        Matcher m = r.matcher(text);
        return m.find();
    }

}

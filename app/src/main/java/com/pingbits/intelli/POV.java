package com.pingbits.intelli;


import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class POV {

    private static String you_re_pattern = "(you are|you're)";

    private static Pattern you_re = Pattern.compile(you_re_pattern, Pattern.CASE_INSENSITIVE);


    public static String change(String text, String username) {
        HashMap<String, String> map = new HashMap<>();
        map.put("i'm", username + " is");
        map.put("i", username);
        map.put("me", username);
        map.put("mine", username + "'s");
        map.put("my", username + "'s");
        map.put("your", "my");
        map.put("you", "me");
        map.put("bring", "take");
        map.put("come", "go");

        int indx = startsWithYoure(text);
        if (indx > 0) {
            return "I am" + abc(text.substring(indx), map);
        } else if (startsWithYou(text)) {
            return "I" + abc(text.substring(3), map);
        } else {
            return abc(text, map);
        }
    }

    private static String abc(String text, HashMap<String, String> map) {
        String[] arr = text.split(" ");
        for (int i = 0; i < arr.length; ++i) {
            String temp = map.get(arr[i].toLowerCase());
            if (temp != null) {
                text = text.replace(arr[i], temp);
            }
        }
        return text;
    }

    public static int startsWithYoure(String text) {
        Matcher m = you_re.matcher(text);
        if (m.find()) {
            return m.end();
        }
        return -1;
    }


    public static Boolean startsWithYou(String text) {
        return text.startsWith("You") || text.startsWith("you");
    }
}

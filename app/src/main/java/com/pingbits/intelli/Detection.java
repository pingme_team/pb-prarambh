package com.pingbits.intelli;


import android.util.Log;

import java.util.Calendar;

public class Detection {

    public static String tag = Detection.class.getSimpleName();


    public static Detected getDetected(String text, String username) {

        boolean isQuestion = Question.isQuestion(text);
        if (isQuestion) {
            Log.w(tag, "this is a question!! FUCK OFF!");
            return null;
        }

        text = text.replace("?", "");

        String day, time, title;
        Calendar cal = Calendar.getInstance();

        Extraction ext = Time.getTime(text);
        time = ext.extract;
        text = ext.text;

        ext = Day.getDay(text);
        day = ext.extract;
        text = ext.text;



        if (time == null && day == null) {
            return null;
        } else {
//            day = "today";
            if (time == null) {
                time = "now";
            } else if (day == null) {
                day = "today";
            }
            title = POV.change(text, username);


            String[] splits = day.split(" ");
            if (splits.length > 1) {
                if (splits[0].equals("this")) {
                    if (splits[1].equals("week")) {
                        int weekday = cal.get(Calendar.DAY_OF_WEEK);
                        if (weekday != Calendar.SUNDAY) {
                            // calculate how much to add
                            // the 2 is the difference between Saturday and Monday
                            int days = (Calendar.SATURDAY - weekday + 2) % 7;
                            cal.add(Calendar.DAY_OF_YEAR, days);
                        }
                    } else if (splits[1].equals("month")) {
                        cal.add(Calendar.MONTH, 1);
                        cal.set(Calendar.DAY_OF_MONTH, 1);
                        cal.add(Calendar.DAY_OF_MONTH, -1);
                    } else if (splits[2].equals("quarter")) {
                        // leave it, it's 32 hrs :P
                    } else if (splits[3].equals("year")) {
                        // leave it, it's 32 hrs :P
                    }
                }
            } else {
//                if (splits[1].equals("week")) {
//                    cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
//                    cal.add(Calendar.DAY_OF_WEEK, 7);
//                } else if (splits[1].equals("month")) {
//                    cal.add(Calendar.MONTH, 1);
//                    cal.set(Calendar.DAY_OF_MONTH, 1);
//                } else if (splits[2].equals("quarter")) {
//                    // leave it, it's 32 hrs :P
//                } else if (splits[3].equals("year")) {
//                    // leave it, it's 32 hrs :P
//                }

                if (day.equals("today") && !time.equals("now")) {
                    time = time.replaceAll("[^\\d]", "");
                    cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time.trim()));
                } else if (day.equals("tomorrow") && !time.equals("now")) {
                    time = time.replaceAll("[^\\d]", "");
                    cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(time.trim()));
                    cal.add(Calendar.DAY_OF_MONTH, 1);
                }
            }
            if (time.length() >= 2 && Character.isDigit(time.charAt(0)) && Character.isDigit(time.charAt(1))) {
                int hours = Integer.parseInt(time.substring(0, 2));
                if (time.contains("p")) {
                    cal.set(Calendar.HOUR_OF_DAY, hours + 12);
                } else {
                    cal.set(Calendar.HOUR_OF_DAY, hours);
                }
            } else if (Character.isDigit(time.charAt(0))) {
                int hours = Character.getNumericValue(time.charAt(0));
                if (time.contains("p")) {
                    cal.set(Calendar.HOUR_OF_DAY, hours + 12);
                } else {
                    cal.set(Calendar.HOUR_OF_DAY, hours);
                }
            }
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND,0);
            cal.set(Calendar.MILLISECOND,0);

            return new Detected(title.trim(), day, time, cal.getTimeInMillis());
        }
    }

}

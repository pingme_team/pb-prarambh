package com.pingbits.intelli;


public class Detected {

    public String day;

    public String time;

    public String title;

    public long timestamp;

    public Detected(String title, String day, String time, long timestamp) {
        this.title = title;
        this.day = day;
        this.time = time;
        this.timestamp = timestamp;
    }


    @Override
    public String toString() {
        return "title: " + title + " | day: " + day + " | time: " + time;
    }
}

package com.pingbits.intelli;


public class Extraction {

    public String extract;

    public String text;

    public Extraction(String extract, String text) {
        this.extract = extract;
        this.text = text;
    }


    @Override
    public String toString() {
        return "Extract : " + extract + "|| Text : " + text;
    }
}

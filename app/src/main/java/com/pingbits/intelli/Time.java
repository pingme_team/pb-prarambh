package com.pingbits.intelli;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Time {

    public static final String tag = Day.class.getSimpleName();

    private static String pattern
            = "(\\d{1,2})[.:](\\d{1,2}) ?(\\.?[a|p]\\.?m\\.?)?|(\\d{1,2}) ?(\\.?[a|p]\\.?m\\.?)";

    private static String pattern2 = "at (\\d{1,2})";

    private static Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

    private static Pattern r2 = Pattern.compile(pattern2, Pattern.CASE_INSENSITIVE);


    public static Extraction getTime(String line) {
        Matcher m = r.matcher(line);
        Matcher m2 = r2.matcher(line);
        StringBuffer sb = new StringBuffer();
        String time = null;
        if (m.find()) {
            time = m.group();
            line = line.replaceAll(pattern, "");
//            m.appendReplacement(sb, "");
            if (hasAtBefore(line, m.start())) {
                line = line.substring(0, m.start() - 3) + line.substring(m.start());
            }
        } else if (m2.find()) {
            time = m2.group();
            line = line.replaceAll(pattern2, "");
//            m.appendReplacement(sb, "");
            if (hasAtBefore(line, m2.start())) {
                line = line.substring(0, m2.start() - 3) + line.substring(m2.start());
            }
        }
//        m.appendTail(sb);
        return new Extraction(time, line);
    }


    private static boolean hasAtBefore(String text, int indx) {
        return (text.charAt(indx - 3) == 'a' || text.charAt(indx - 3) == 'A') && (
                text.charAt(indx - 2) == 't') && (text.charAt(indx - 1) == ' ');
    }
}

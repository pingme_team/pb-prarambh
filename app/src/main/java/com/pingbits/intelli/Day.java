package com.pingbits.intelli;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day {

    public static final String tag = Day.class.getSimpleName();

    private static String pattern
            = "(tonight|today|tomorrow)|(this|next|upcoming|following|coming) (week|month|quarter|year)";

    private static Pattern r = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);


    public static Extraction getDay(String line) {
        Matcher m = r.matcher(line);
        StringBuffer sb = new StringBuffer();
        String day = null;
        if (m.find()) {
            day = m.group();
            m.appendReplacement(sb, "");
        }
        m.appendTail(sb);
        return new Extraction(day, sb.toString());
    }
}

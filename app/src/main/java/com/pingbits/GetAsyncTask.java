package com.pingbits;


import android.app.Activity;
import android.os.AsyncTask;

import java.util.ArrayList;

public class GetAsyncTask extends AsyncTask<Void, ArrayList<MsgObject>, Void> {

    public Communicator cc;

    private Activity activity;


    public GetAsyncTask(WelcomeChat activity) {
        this.activity = activity;
        cc = new Communicator(activity);
    }


    @Override
    protected Void doInBackground(Void... voids) {
        int i = 1;
        while (i > 0) {
            ArrayList<MsgObject> msgs = cc.getMessages();
            publishProgress(msgs);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    @Override
    protected void onProgressUpdate(ArrayList<MsgObject>... values) {
        ArrayList<MsgObject> msgs = values[0];
        ((WelcomeChat) activity).addNewMessages(msgs);
    }
}

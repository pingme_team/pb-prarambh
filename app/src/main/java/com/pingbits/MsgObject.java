package com.pingbits;


public class MsgObject {

    public String _id;

    public String sender;

    public String text;


    public MsgObject(String sender, String text) {
        this.sender = sender;
        this.text = text;
    }
}

package com.pingbits;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.pingbits.adapters.ChatListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aagam on 21/2/15.
 */
public class ChatListFragment extends Fragment {

    public ListView lv;
    public ChatListAdapter chatListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_chat,container,false);
        lv = (ListView)view.findViewById(R.id.chat_list);
        lv.setTranscriptMode(ListView.TRANSCRIPT_MODE_NORMAL);
        lv.setStackFromBottom(true);
        List<MsgObject> obj = new ArrayList<>();

        SharedPreferences preferences = getActivity().getSharedPreferences("pb", Context.MODE_PRIVATE);
        String user_name = preferences.getString("name","null1");
        chatListAdapter = new ChatListAdapter(getActivity(),obj,user_name);
        lv.setAdapter(chatListAdapter);
        return view;
    }
}

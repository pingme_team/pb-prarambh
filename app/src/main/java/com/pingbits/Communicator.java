package com.pingbits;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.util.Log;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class Communicator {

    private final String tag = getClass().getSimpleName();

    private final WelcomeChat activity;

    private AtomicBoolean started = new AtomicBoolean(false);

    private final String sync_obj = "";

    private String last_id = "";


    public Communicator(WelcomeChat activity) {
        this.activity = activity;
    }


    private Gson gson = new Gson();


    public void sendMessage(String name, String text) {
        RequestParams params = new RequestParams();
        params.add("name", name);
        params.add("text", text);
        Client.post("/send", params, new TextHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseStr) {
                Log.d(tag, "send response : " + responseStr);
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString,
                    Throwable throwable) {
            }
        });
    }


    public ArrayList<MsgObject> getMessages() {
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost("http://192.168.4.210:3000/get");
        try {
            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<>(1);
            nameValuePairs.add(new BasicNameValuePair("_id", last_id));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            String responseStr = EntityUtils.toString(response.getEntity());
            Log.d(tag, responseStr);
            ArrayList<MsgObject> msgs = decodeJson(responseStr);
            if (msgs.size() > 0) {
                last_id = msgs.get(msgs.size() - 1)._id;
            }
            return msgs;
        } catch (IOException ignored) {
        }
        return null;
    }


    private ArrayList<MsgObject> decodeJson(String msgs) {
        Type msgList = new TypeToken<ArrayList<MsgObject>>() {
        }.getType();
        return gson.fromJson(msgs, msgList);
    }


    /**
     * The first letter should be a slash
     */
    private String getAbsoluteUrl(String relative) {
        return Client.BASE_URL + relative;
    }
}
